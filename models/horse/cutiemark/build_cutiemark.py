"""
Script to clean + copy horse cutiemark shape part json model files.
Need to:
1. change texture names to use emblem, change element texture
   name targets
"""

import argparse
import json
import os

# parse first arg as full filepath of exported json file
parser = argparse.ArgumentParser()
parser.add_argument("path_in_json", help="path to input json file")
args = parser.parse_args()

path_in_json = args.path_in_json
filename = os.path.basename(path_in_json)
folder = os.path.dirname(path_in_json)

if not os.path.exists(path_in_json):
    print(f"ERROR: Path to input json does not exist: {path_in_json}")
    exit(1)

def find_local_mod_folder():
    path_mods = os.path.join(os.getenv("APPDATA"), "VintagestoryData", "Mods")
    if not os.path.exists(path_mods):
        raise Exception(f"ERROR: Path to mod folder does not exist: {path_mods}")
    # stupidly tick version until we find the right folder trololo
    for i in range(100):
        path_mod = os.path.join(path_mods, f"kemono_0.0.{i}")
        if os.path.exists(path_mod):
            return path_mod
    raise Exception(f"ERROR: Could not find mod folder in {path_mods}, is it named kemono_0.0.X?")

### TODO: easier way to select output path, mod dev folder or AppData folder
### for mod testing:
# path_out_base = os.path.join(folder, "..", "..", "..", "resources")
### game mod folder:
path_out_base = find_local_mod_folder()

path_out = os.path.join(path_out_base, "assets", "kemono", "shapes", "entity", "horse", "skinparts", "cutiemark")
path_out_json = os.path.join(path_out, filename)

with open(path_in_json, "r") as f:
    model = json.load(f)

# change textures and texture sizes
model["textures"] = {
    "cutiemark": "kemono:entity/common/emblem",
}
model["textureSizes"] = {
    "cutiemark": [32, 32],
}

# walk elements, change texture name targets
# (do not need recursive, these models will only be 1 level deep)
for element in model["elements"]:
    for face in element["faces"].values():
        face["texture"] = "#cutiemark"
        
        is_active_face = False
        for uv in face["uv"]: # coords [u0, v0, u1, v1]
            # active faces map whole uv, with coords (0, 0) to (32, 32)
            # invalid faces are mapped (0, 0) to (4, 4)
            # so if any face has a ~32 uv coord, its the active
            # while other faces need to be disabled
            if uv > 31:
                is_active_face = True
                break
        
        # disable unused faces
        if not is_active_face:
            face["enabled"] = False

# write to output
with open(path_out_json, "w+") as f:
    json.dump(model, f, indent=2)
