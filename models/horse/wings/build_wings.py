"""
Script to clean + copy horse shape part json model files.
"""
import json
import os
import argparse

# parse first arg as full filepath of exported json file
parser = argparse.ArgumentParser()
parser.add_argument("path_in_json", help="path to input json file")
args = parser.parse_args()

path_in_json = args.path_in_json
filename = os.path.basename(path_in_json)
folder = os.path.dirname(path_in_json)

if not os.path.exists(path_in_json):
    print(f"ERROR: Path to input json does not exist: {path_in_json}")
    exit(1)

def find_local_mod_folder():
    path_mods = os.path.join(os.getenv("APPDATA"), "VintagestoryData", "Mods")
    if not os.path.exists(path_mods):
        raise Exception(f"ERROR: Path to mod folder does not exist: {path_mods}")
    # stupidly tick version until we find the right folder trololo
    for i in range(100):
        path_mod = os.path.join(path_mods, f"kemono_0.0.{i}")
        if os.path.exists(path_mod):
            return path_mod
    raise Exception(f"ERROR: Could not find mod folder in {path_mods}, is it named kemono_0.0.X?")

### TODO: easier way to select output path, mod dev folder or AppData folder
### for mod testing:
# path_out_base = os.path.join(folder, "..", "..", "..", "resources")
### game mod folder:
path_out_base = find_local_mod_folder()

path_out = os.path.join(path_out_base, "assets", "kemono", "shapes", "entity", "horse", "skinparts", "wings")
path_out_json = os.path.join(path_out, filename)

with open(path_in_json, "r") as f:
    model = json.load(f)

# change textures and texture sizes
model["textures"] = {
    
}
model["textureSizes"] = {
    
}

# taken from blender script
BONE_POSITIONS = {
    "b_WingL": [-1.4993185997009277, 13.354035377502441, 2.334045171737671],
    "b_WingInner3L": [-1.4956977367401123, 13.091535568237305, 2.6564717292785645],
    "b_WingInner2L": [-1.5051653385162354, 12.86171817779541, 2.859478712081909],
    "b_WingInner1L": [-1.4806525707244873, 11.739184379577637, 3.1970462799072266],
    "b_WingInner0L": [-1.50498366355896, 10.7901611328125, 3.395319938659668],
    "b_WingR": [-1.4993185997009277, 13.354035377502441, -2.334045171737671],
    "b_WingInner3R": [-1.4956977367401123, 13.091535568237305, -2.6564717292785645],
    "b_WingInner2R": [-1.5051653385162354, 12.86171817779541, -2.859478712081909],
    "b_WingInner1R": [-1.4806525707244873, 11.739184379577637, -3.1970462799072266],
    "b_WingInner0R": [-1.50498366355896, 10.7901611328125, -3.395319938659668],
}

# recursively walk elements, change texture name targets
def modify_element(elem):
    # modify positions to be relative to attach point (stepParent)
    parent_pos = BONE_POSITIONS[elem["stepParentName"]]
    elem["from"] = [
        elem["from"][0] - parent_pos[0],
        elem["from"][1] - parent_pos[1],
        elem["from"][2] - parent_pos[2],
    ]
    elem["to"] = [
        elem["to"][0] - parent_pos[0],
        elem["to"][1] - parent_pos[1],
        elem["to"][2] - parent_pos[2],
    ]
    elem["rotationOrigin"] = [
        elem["rotationOrigin"][0] - parent_pos[0],
        elem["rotationOrigin"][1] - parent_pos[1],
        elem["rotationOrigin"][2] - parent_pos[2],
    ]

    # modify face texture targets
    for face in elem["faces"].values():
        face["texture"] = "#seraph"
    
    for child in elem["children"]:
        modify_element(child)

# walk root elements
for element in model["elements"]:
    modify_element(element)

# write to output
with open(path_out_json, "w+") as f:
    json.dump(model, f, indent=2)
