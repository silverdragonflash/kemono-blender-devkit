"""
Script to clean + copy horse shape part json model files.
"""

import json
import os
import argparse

# parse first arg as full filepath of exported json file
parser = argparse.ArgumentParser()
parser.add_argument("path_in_json", help="path to input json file")
args = parser.parse_args()

path_in_json = args.path_in_json
filename = os.path.basename(path_in_json)
folder = os.path.dirname(path_in_json)

if not os.path.exists(path_in_json):
    print(f"ERROR: Path to input json does not exist: {path_in_json}")
    exit(1)

def find_local_mod_folder():
    path_mods = os.path.join(os.getenv("APPDATA"), "VintagestoryData", "Mods")
    if not os.path.exists(path_mods):
        raise Exception(f"ERROR: Path to mod folder does not exist: {path_mods}")
    # stupidly tick version until we find the right folder trololo
    for i in range(100):
        path_mod = os.path.join(path_mods, f"kemono_0.0.{i}")
        if os.path.exists(path_mod):
            return path_mod
    raise Exception(f"ERROR: Could not find mod folder in {path_mods}, is it named kemono_0.0.X?")

### TODO: easier way to select output path, mod dev folder or AppData folder
### for mod testing:
# path_out_base = os.path.join(folder, "..", "..", "..", "resources")
### game mod folder:
path_out_base = find_local_mod_folder()

path_out = os.path.join(path_out_base, "assets", "kemono", "shapes", "entity", "horse", "skinparts", "tail")
path_out_json = os.path.join(path_out, filename)

with open(path_in_json, "r") as f:
    model = json.load(f)

step_parent_name="b_Tail0"

# change textures and texture sizes
model["textures"] = {
    "tail": "kemono:entity/horse/tail/horse-tail",
}
model["textureWidth"] = 64
model["textureHeight"] = 64

# recursively walk elements, change texture name targets
def modify_element(elem):
    # add stepParentName (attachment point)
    elem["stepParentName"] = step_parent_name

    ### modify face texture targets
    # NO LONGER NEEDED
    # for face in elem["faces"].values():
    #     face["texture"] = texture_remapping[face["texture"]]
    
    for child in elem["children"]:
        modify_element(child)

# walk root elements
for element in model["elements"]:
    modify_element(element)

# write to output
with open(path_out_json, "w+") as f:
    json.dump(model, f, indent=2)