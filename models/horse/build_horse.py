"""
Script to copy + clean base character models into mod resources.
Need to:
1. change texture names/sizes

TODO: more adjustments future
"""

import json
import os
from dataclasses import dataclass
import argparse

# parse first arg as full filepath of exported json file
parser = argparse.ArgumentParser()
parser.add_argument("path_in_json", help="path to input json file")
args = parser.parse_args()

path_in_json = args.path_in_json
filename = os.path.basename(path_in_json)
folder = os.path.dirname(path_in_json)

if not os.path.exists(path_in_json):
    print(f"ERROR: Path to input json does not exist: {path_in_json}")
    exit(1)

def find_local_mod_folder():
    path_mods = os.path.join(os.getenv("APPDATA"), "VintagestoryData", "Mods")
    if not os.path.exists(path_mods):
        raise Exception(f"ERROR: Path to mod folder does not exist: {path_mods}")
    # stupidly tick version until we find the right folder trololo
    for i in range(100):
        path_mod = os.path.join(path_mods, f"kemono_0.0.{i}")
        if os.path.exists(path_mod):
            return path_mod
    raise Exception(f"ERROR: Could not find mod folder in {path_mods}, is it named kemono_0.0.X?")

### TODO: easier way to select output path, mod dev folder or AppData folder
### for mod testing:
# path_out_base = os.path.join(folder, "..", "..", "resources")
### game mod folder:
path_out_base = find_local_mod_folder()

path_out_json = os.path.join(path_out_base, "assets", "kemono", "shapes", "entity", "horse", "main", "horse0.json")

if not os.path.exists(path_out_base):
    print(f"ERROR: Path to output base does not exist: {path_out_base}")
    exit(1)

# load model
with open(path_in_json, "r") as f:
    model = json.load(f)

# change textures and texture sizes
model["textures"] = {}
model["textureSizes"] = {}

# add element scaleX, scaleY, scaleZ properties
# use this to adjust scale of dummy elements which are used to
# position wearable item attachments (e.g. armor, clothes),
# which scales size of attachment
element_scales = { # (scaleX, scaleY, scaleZ)
    "LowerFootR": (0.8, 0.8, 0.8),
    "LowerFootL": (0.8, 0.8, 0.8),
    "UpperFootR": (1.0, 0.9, 0.9),
    "UpperFootL": (1.0, 0.9, 0.9),
    "LowerArmL": (1.1, 1.1, 1.1),
    "LowerArmR": (1.1, 1.1, 1.1),
    "UpperArmL": (1.1, 1.1, 1.1),
    "UpperArmR": (1.1, 1.1, 1.1),
    "LowerTorso": (1.0, 1.0, 1.0),
    "UpperTorso": (1.0, 1.0, 1.0),
    "Neck": (1.1, 1.1, 1.1),
    "Head": (1.1, 1.1, 1.1),
}

def modify_element(element):
    """Recursively modify/adjust elements"""
    if element["name"] in element_scales:
        scaleX, scaleY, scaleZ = element_scales[element["name"]]
        element["scaleX"] = scaleX
        element["scaleY"] = scaleY
        element["scaleZ"] = scaleZ
    
    if "children" in element:
        for child in element["children"]:
            modify_element(child)

for element in model["elements"]:
    modify_element(element)

# write to output
with open(path_out_json, "w+") as f:
    print(f"Writing: {path_out_json}")
    json.dump(model, f, indent=2)